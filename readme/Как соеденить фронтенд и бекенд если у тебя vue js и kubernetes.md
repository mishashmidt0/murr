Привет!
Меня зовут Егор Комаров. В прошлой статье (ссылка на gitlab runner) мы сделали наше приложение доступным во всем мире. 

Но человек так устроен, что мурренган.ру запомнить легче, чем 91.185.95.26. 

Поэтому в интернете принято вести таблицу имен и ip адресов. К примеру когда ты заходишь на yandex.ru ты на самом деле идешь на 5.255.255.60 или 77.88.55.77 и этих адресов много.
Конкретно я работаю с reg.ru. Там можно купить домен по своему желанию. Выглядит по итогу это вот так:
Теперь мне нужно получить ip адрес своего приложения.Сегодня я возьму чистый кластер у  #CloudMTS. Сервис предоставляет набор готовых решений, в частности выделит мне в управление ноду в своей инфраструктуре и возможность получать трафик из интернета. 
На выходе у меня будет куб конфиг, который закину в ~/.kube/Проверю сервисы в дефолтном неймспейсе:
kubectl get svc
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   34m
Установлю ингресс контроллер nginx https://kubernetes.github.io/ingress-nginx/deploy/



kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.2/deploy/static/provider/cloud/deploy.yaml


Дождусь выдачи ip 
egor@MacBook-Air-Egor murr_server % kubectl get svc -n ingress-nginx 

NAME                                 TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                      AGE
ingress-nginx-controller             LoadBalancer   10.96.53.99    91.185.84.118   80:32451/TCP,443:31739/TCP   48s

Пропишу полученный ip 91.185.84.118 в рег ру
Роутеру может потребоваться до 24 часов для обновления записи.
Или у вас закешировался dns на роуте и нужно почистить 
Проверить, какой у вас система видит dns у murrengan.ru можно через 
dig murrengan.ru

Смотреть пункт:

;; ANSWER SECTION:
murrengan.ru.           12203   IN      A       91.185.95.27


Опишу манифест приложения – manifest.yaml


apiVersion: apps/v1
kind: Deployment
metadata:
  name: murr-server-deployment
spec:
  selector:
    matchLabels:
      app: murr-server
  replicas: 3
  template:
    metadata:
      labels:
        app: murr-server
    spec:
      containers:
        - name: murr-server
          image: registry.gitlab.com/murrengan/murr_server:2ca1c000
          imagePullPolicy: Always
          ports:
            - containerPort: 1991

Создам лоад-балансер для получения трафика из интернета. Load balancer позволяет получить трафик из интеренета в свое приложение на 4 уровне osi - TCP:
apiVersion: v1
kind: Service
metadata:
  name: murr-proxy-lb
  annotations:
    test_annotations_key: "test_annotations_value"
spec:
  selector:
    app: murr-proxy
  externalTrafficPolicy: Cluster
externalTrafficPolicy: local
  type: LoadBalancer
  ports:
    - port: 8000
      targetPort: 8000
      protocol: TCP

Применю конфиг и дождусь выдачи ip адреса
Kubectl apply –f 
