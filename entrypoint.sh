#!/bin/sh

if [ "$DJANGO_DB_ENGINE" = "django.db.backends.postgresql" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $DJANGO_DB_HOST $DJANGO_DB_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

python manage.py migrate
python manage.py collectstatic --no-input --clear
gunicorn murr_back.wsgi:application --bind 0.0.0.0:8000

#echo "* * * * * env YANDEX_DISK_TOKEN=${YANDEX_DISK_TOKEN} /usr/local/bin/python3 /home/murrengan/backup.py" >> mycron
#crontab mycron
#rm mycron
#service cron start

exec "$@"
