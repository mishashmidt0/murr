import channels.layers
from asgiref.sync import async_to_sync
from django.db.models.signals import post_save
from django.dispatch import receiver

from murr_notifications.models import FeedNotification, SubscribeNotification
from murr_notifications.serializers import (
    FeedNotificationSerializer,
    SubscribeNotificationSerializer,
)


def new_notification(notify):
    channel_layer = channels.layers.get_channel_layer()
    group_name = f'service_notifications_{notify["to_murren"]}'

    async_to_sync(channel_layer.group_send)(
        group_name,
        {
            'type': 'gan__send_new_notification',
            'gan': 'send_new_notification',
            'data': notify
        }
    )


@receiver(post_save, sender=FeedNotification)
def new_feed_notification(sender, instance, created, **kwargs):
    if created:
        new_notification(FeedNotificationSerializer(instance).data)


@receiver(post_save, sender=SubscribeNotification)
def new_subscribe_notification(sender, instance, created, **kwargs):
    if created:
        new_notification(SubscribeNotificationSerializer(instance).data)
