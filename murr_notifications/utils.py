from itertools import chain

from murr_notifications.models import BaseNotification
from murr_notifications.serializers import (
    FeedNotificationSerializer,
    SubscribeNotificationSerializer,
)


def serialize_notification(notify, request=None):
    serializer = {
        'feed': FeedNotificationSerializer,
        'subscribe': SubscribeNotificationSerializer,
    }.get(notify.notification_type.value)
    return serializer(notify, context={'request': request}).data


def get_notifications(user):
    queryset = BaseNotification.objects.filter(to_murren=user)
    feed_notifications = queryset.filter(feednotification__isnull=False)
    subscribe_notifications = queryset.filter(subscribenotification__isnull=False)

    notifications = sorted(
        chain(
            feed_notifications,
            subscribe_notifications
        ),
        key=lambda x: x.sent_time,
        reverse=True
    )
    return notifications
