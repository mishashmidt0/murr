from django.db.models.signals import post_save
from django.dispatch import receiver

from murr_card.models import MurrCard
from murr_notifications.models import FeedNotification


@receiver(post_save, sender=MurrCard)
def new_murr_card(sender, instance, created, **kwargs):
    if created:
        owner = instance.owner
        for murren in owner.subscribers.all():
            if murren.show_murr_card_notifications:
                FeedNotification.objects.create(to_murren=murren, murr_card=instance)
