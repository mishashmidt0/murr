from django.contrib import admin
from .models import MurrWebSocket, MurrWebSocketMembers

admin.site.register(MurrWebSocketMembers)


@admin.register(MurrWebSocket)
class MWSAdmin(admin.ModelAdmin):
    readonly_fields = ('murr_ws_member',)
    fields = ('murr_ws_name', 'murr_ws_type', 'murr_ws_member')

    @staticmethod
    def murr_ws_member(obj):
        return list(obj.murr_ws_member.values_list('murr_ws_member__username', flat=True))
