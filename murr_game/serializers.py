from django.conf import settings
from rest_framework import serializers

from murr_game.models import Player


class PlayerSerializer(serializers.ModelSerializer):
    total_won = serializers.IntegerField()
    total_games = serializers.IntegerField()

    class Meta:
        model = Player
        fields = (
            'murren_player',
            'available_points',
            'intelligence',
            'strength',
            'dexterity',
            'vitality',
            'total_won',
            'total_games',
        )
        read_only_fields = ('murren_player', 'available_points', 'total_won', 'total_games')


class PlayerLeaderBoardSerializer(serializers.ModelSerializer):
    total_won = serializers.IntegerField()
    total_games = serializers.IntegerField()
    murren_player = serializers.StringRelatedField(read_only=True)
    avatar = serializers.SerializerMethodField()

    def get_avatar(self, obj):
        return f'{settings.BACKEND_URL}{obj.murren_player.murren_avatar.url}'

    class Meta:
        model = Player
        fields = (
            'murren_player',
            'avatar',
            'total_won',
            'total_games',
        )
        read_only_fields = ('murren_player', 'avatar', 'total_won', 'total_games')
