from django.core.management import BaseCommand

from murr_game.models import Skills, SkillType, Murren, Player


class Command(BaseCommand):
    help = 'Create skills and add it to existing Murren / Создать скилы и добавить существующим Мурренам'

    def handle(self, *args, **options):
        strike, _ = Skills.objects.get_or_create(name='strike', cost=2, base_damage=2, animation_cooldown=500,
                                                 icon='./murr_game/icon/strike.jpg', reload=0,
                                                 description='Ударить противника концентрируясь на своих сильных сторонах.',
                                                 logic='Модификатор урона от большей характеристики',
                                                 gif='./murr_game/gif/strike.gif', type=SkillType.INDEPENDENT)
        backstab, _ = Skills.objects.get_or_create(name='backstab', cost=2, base_damage=3, animation_cooldown=500,
                                                   icon='./murr_game/icon/backstab.jpg', reload=2,
                                                   description='Подлый удар в спину с повышенным шансом критического удара и игнорированием брони.',
                                                   logic='Уменьшение брони цели на 50%, Увеличить шанс критического удара +15%',
                                                   gif='./murr_game/gif/backstab.gif', type=SkillType.PHYSICAL)
        freezing, _ = Skills.objects.get_or_create(name='freezing', cost=3, base_damage=10, animation_cooldown=500,
                                                   icon='./murr_game/icon/freezing.jpg', reload=3,
                                                   description='Метнуть в противника ледяной шар.',
                                                   logic='Игнорирует броню цели.',
                                                   gif='./murr_game/gif/freezing.gif', type=SkillType.MAGICAL)
        power_strike, _ = Skills.objects.get_or_create(name='power_strike', cost=2, base_damage=4,
                                                       animation_cooldown=1000,
                                                       icon='./murr_game/icon/power_strike.jpg', reload=2,
                                                       description='Яростный, но безрассудный выпад.',
                                                       logic='Противнику легче уклониться на 10%',
                                                       gif='./murr_game/gif/power_strike.gif', type=SkillType.PHYSICAL)
        skip, _ = Skills.objects.get_or_create(name='skip', cost=0, base_damage=0, animation_cooldown=1000,
                                               icon='./murr_game/icon/skip.jpg', reload=0,
                                               description='Подождать и восстановить силы.', logic='Пропуск хода.',
                                               gif='./murr_game/gif/skip.gif', self_auto_use=True,
                                               sensitive_to_evasion=False, type=SkillType.INDEPENDENT)

        for murren in Murren.objects.all():
            if not murren.is_superuser:
                player = Player.objects.get(murren_player=murren)
                player.skills.add(strike, backstab, freezing, power_strike, skip)
                player.save()
