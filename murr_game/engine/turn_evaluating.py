import json
from random import randrange

from murr_game import mpv
from murr_game.engine.check_winner_group import check_winner_group
from murr_game.engine.create_game_history import create_game_history
from murr_game.engine.defeated_murren_logic import set_defeated_murren
from murr_game.engine.skill_impact.skill import *

"""
I don't believe data from frontend and try to protect backend by working with
saved data in redis (room_data), whom created and saved before every battle.
"""


class TurnEvaluating:
    def __init__(self, group_send, scope, gan, murr_redis, murr_logger):
        # service
        self.murr_redis = murr_redis
        self.group_send = group_send
        self.murr_logger = murr_logger
        self.room_data = json.loads(murr_redis.get(gan['data']['murr_game_room_instance']))

        # variables
        self.damage = None
        self.gan_name = gan['gan']
        self.room_name = scope['room_name']
        self.room_data['temporary_skill_data'] = {}
        self.chosen_skill_name = gan['data']['chosen_skill']['name']
        self.now_turn_group = self.room_data['turn_list'][0]['murren_group']
        self.murr_game_room_instance = gan['data']['murr_game_room_instance']
        self.now_turn_murren_name = self.room_data['turn_list'][0]['murren_name']
        self.target_murren_name = gan['data']['target_murren']['murren_player_data']['murren_name']
        self.target_murren_group = gan['data']['target_murren']['murren_player_data']['murren_group']

        # def
        self.now_turn_murren_data = self.get_now_turn_murren_data()
        self.set_target_murren_data()
        self.chosen_skill_data = self.get_chosen_skill_data()
        self.check_for_can_execute()
        self.skill = prepare_chosen_skill(self.now_turn_murren_data, self.room_data, self.chosen_skill_name)

    def set_target_murren_data(self):
        for m in self.room_data[self.target_murren_group]:
            if m['murren_player_data']['murren_name'] == self.target_murren_name:
                self.room_data['target_murren'] = m['murren_player_data']

    def get_chosen_skill_data(self):
        for skill in self.now_turn_murren_data['skills']:
            if skill['name'] == self.chosen_skill_name:
                return skill

    def check_for_can_execute(self):
        if self.now_turn_murren_data['turn_points'] - self.chosen_skill_data['cost'] < 0 \
                or self.chosen_skill_data.get('now_reloading'):
            self.room_data['temporary_skill_data']['can_execute'] = False

    def get_now_turn_murren_data(self):
        for murren in self.room_data[self.now_turn_group]:
            if murren['murren_player_data']['murren_name'] == self.now_turn_murren_name:
                return murren['murren_player_data']

    async def evasion_eval(self):
        skill_sensitive_to_evasion = self.chosen_skill_data['sensitive_to_evasion']
        target_murren_evasion = self.room_data['target_murren']['evasion']

        if skill_sensitive_to_evasion:
            evasion = await self.skill.evasion(target_murren_evasion)
            successful_evasion_values = [x for x in range(1, evasion + 1)]
            dice = randrange(1, 101)
            if dice in successful_evasion_values:
                self.skill.change_temporary_skill_data({'css_animation': 'successful_evasion',
                                                        'gif': '', 'damage': 0})
                return False
        return 'evasion_failed'

    async def can_execute(self):
        if self.room_data['temporary_skill_data'].get('can_execute') is False:
            self.skill.change_temporary_skill_data({'css_animation': '', 'gif': '', 'damage': 0})
            await self.group_send(self.room_data, gan=self.gan_name, group=self.room_name)
            return False

    def decrease_now_reloading_skill(self):
        decrease_reloading_value_for_turn = 1
        for skill in self.now_turn_murren_data['skills']:
            if skill.get('now_reloading'):
                skill['now_reloading'] -= decrease_reloading_value_for_turn

    def recovery(self):
        self.decrease_now_reloading_skill()
        self.now_turn_murren_data['turn_points'] += mpv['RESTORE_TURN_POINTS']
        if self.now_turn_murren_data['turn_points'] > mpv['MAX_TURN_POINTS']:
            self.now_turn_murren_data['turn_points'] = mpv['MAX_TURN_POINTS']

    def turn_auditor(self):
        self.now_turn_murren_data['turn_points'] -= self.chosen_skill_data['cost']
        self.chosen_skill_data['now_reloading'] = self.chosen_skill_data['reload']
        if self.now_turn_murren_data['turn_points'] == 0 or self.chosen_skill_name == 'skip':
            self.recovery()
            self.room_data['turn_list'].append(self.room_data['turn_list'].pop(0))

    async def skill_eval(self):
        init_skill_damage = self.chosen_skill_data['init_damage']
        base_damage = await self.skill.damage(init_skill_damage)
        damage = await self.skill.critical_chance(base_damage)
        if self.chosen_skill_data['self_auto_use']:
            self.skill.change_temporary_skill_data({'css_animation': 'skip'})
        self.skill.change_temporary_skill_data({'damage': damage,
                                                'gif': self.chosen_skill_data['gif']})
        self.damage = damage

    async def armor_eval(self):
        armor = await self.skill.armor(self.room_data['target_murren']['armor'])
        self.damage = math.ceil(self.damage * (100 / (100 + armor)))

    def set_damage(self):
        self.skill.change_temporary_skill_data({'damage': self.damage})
        self.room_data['target_murren']['hp'] -= self.damage

    async def check_defeated_murren(self):
        if self.room_data['target_murren']['hp'] <= 0:
            await set_defeated_murren(self.room_data, self.room_data['target_murren'])
            await check_winner_group(self.room_data)
            await create_game_history(self.room_data)

    async def run(self):
        if await self.can_execute() is False:
            return

        self.turn_auditor()

        if await self.evasion_eval() == 'evasion_failed':
            await self.skill_eval()
            await self.armor_eval()
            self.set_damage()
        await self.check_defeated_murren()
        self.murr_logger('murr_game_turn', {'room_data': self.room_data})
        self.murr_redis.set(self.murr_game_room_instance, json.dumps(self.room_data))
        await self.group_send(self.room_data, gan=self.gan_name, group=self.room_name)
