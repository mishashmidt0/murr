import math


def init_damage_by_type(damage_type, mpv, murren_stats, skill_base_damage):
    damage_type_values = {
        'physical': physical,
        'magical': magical,
        'independent': independent
    }
    return damage_type_values[damage_type](mpv, murren_stats, skill_base_damage)


def physical(mpv, murren_stats, skill_base_damage):
    max_damage = skill_base_damage + math.ceil(
        mpv["MIN_PHYSICAL_DAMAGE"] + murren_stats['strength'] / mpv["PHYSICAL_DAMAGE_MULTIPLY"])
    min_damage = math.ceil(max_damage - (max_damage / 100) * mpv["MAX_PHYSICAL_DAMAGE_MULTIPLY"])

    return {
        'max_damage': max_damage,
        'min_damage': min_damage,
    }


def magical(mpv, murren_stats, skill_base_damage):
    return {
        'max_damage': skill_base_damage,
        'min_damage': skill_base_damage,
    }


def independent(mpv, murren_stats, skill_base_damage):
    return {
        'max_damage': skill_base_damage,
        'min_damage': skill_base_damage,
    }
