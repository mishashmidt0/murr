from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

from murr_game.models import Player, Skills

Murren = get_user_model()


@receiver(post_save, sender=Murren)
def new_murren_player(sender, instance, created, **kwargs):
    if created:
        player = Player.objects.create(murren_player=instance)
        skills_name = 'strike', 'backstab', 'freezing', 'power_strike', 'skip'
        for name in skills_name:
            skill = Skills.objects.filter(name=name).first()
            player.skills.add(skill)
            player.save()
