import axios from "axios";

/**
 * Fetch character stats by username
 *
 * @param {String} username Murren username
 *
 * @return {Promise<String|Object>}
 */
export const findStatsCharacterByUsername = async (username) => {
  try {
    const { data } = await axios.get(`/api/murr_game/${username}/`);

    return data;
  } catch {
    return Promise.reject(
      "Произошла ошибка при получении характеристик персонажа"
    );
  }
};

/**
 * Update character stats by username
 *
 * @param {String} username Murren username
 * @param {Object} payload  Stats payload
 *
 * @return {Promise<String|Object>}
 */
export const updateStatsCharacterByUsername = async (username, payload) => {
  try {
    const { data } = await axios.patch(`/api/murr_game/${username}/`, payload);

    return data;
  } catch {
    return Promise.reject(
      "Произошла ошибка при сохранении характеристик персонажа"
    );
  }
};

/**
 * Reset character stats by username
 *
 * @param {String} username Murren username
 *
 * @return {Promise<String|Object>}
 */
export const resetStatsCharacterByUsername = async (username) => {
  try {
    const { data } = await axios.post(
      `/api/murr_game/${username}/reset_stats/`
    );

    return data;
  } catch {
    return Promise.reject(
      "Произошла ошибка при сбросе характеристик персонажа"
    );
  }
};
