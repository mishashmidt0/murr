import axios from "axios";

/**
 * @param {Number} murrId Murren card id
 * @param {Number} page Number page
 */
export const fetchComments = async (murrId, page) => {
  try {
    const { data, status } = await axios.get(
      `/api/murr_comments/?murr=${murrId}&page=${page}`
    );

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};

/**
 * @param {Object} formData Object data form
 */
export const addComment = async (formData) => {
  try {
    const { data, status } = await axios.post(`/api/murr_comments/`, formData);

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};

/**
 * @param {Number} id Comment id
 */
export const deleteComment = async (id) => {
  try {
    const { data, status } = await axios.delete(`/api/murr_comments/${id}/`);

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};

/**
 * @param {Number} id Comment id
 */
export const likeComment = async (id) => {
  try {
    const { data, status } = await axios.post(`/api/murr_comments/${id}/like/`);

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};

/**
 * @param {Number} id Comment id
 */
export const unlikeComment = async (id) => {
  try {
    const { data, status } = await axios.post(
      `/api/murr_comments/${id}/dislike/`
    );

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};
