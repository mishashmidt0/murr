export const axios_defaults_baseURL =
  process.env.NODE_ENV === "production"
    ? "http://murrengan.ru"
    : "http://127.0.0.1:8000";
export const axios_defaults_baseFrontURL =
  process.env.NODE_ENV === "production"
    ? "http://murrengan.ru"
    : "http://127.0.0.1:8080";

export const websocket_base_url =
  process.env.NODE_ENV === "production"
    ? "ws://murrengan.ru"
    : "ws://127.0.0.1:8000";
// export const axios_defaults_baseURL =
//   process.env.NODE_ENV === "production"
//     ?  "http://91.185.95.8:8000"
//     :  "http://91.185.95.8:8000";
// export const axios_defaults_baseFrontURL =
//   process.env.NODE_ENV === "production"
//     ?  "http://91.185.95.8:8000"
//     :  "http://91.185.95.8:8000";
//
// export const websocket_base_url =
//   process.env.NODE_ENV === "production"
//     ? "ws://91.185.95.8:8000"
//     :  "ws://91.185.95.8:8000";

export const notificationsSocketUrl = "/ws/service/notifications/";
export const tavernSocketUrl = "/ws/murr_chat/murr_tavern/";
export const battleLobbyUrl = "/ws/murr_battle_room/lobby/";
export const murrWebSocketTypes = {
  MURR_CHAT: "murr_chat",
  MURR_BATTLE_ROOM: "murr_battle_room",
};

export const siteKey = "6Lc0qP4UAAAAADXDdZYU_wnfeDJfTRKRPVU9h046";
export const murrOauthGoogle =
  "377784541308-idfr0lv3ia1fnturl2hrtgmbbq98shnm.apps.googleusercontent.com";
// export const DISABLE_RECAPTCHA = process.env.NODE_ENV !== "production";
export const DISABLE_RECAPTCHA = true;

export const VK_CLIENT_ID = "7652183";
export const VK_REDIRECT_URI = `${axios_defaults_baseFrontURL}/oauth/vk`;

export const FB_CLIENT_ID = "689615251746843";
